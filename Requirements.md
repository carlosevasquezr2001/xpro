# Requirements
Prepare a file called _info.txt_ with following informations : 
## Server infos
 - Server website
 - Server IP
 - Server Name
 - Server Port (eg. 6900)
 - Langtype (default 12)
 - Packet version (default 20180620)
 - rAthena or Hercules?
 - Renewal or Pre-re?
 - OS/Distribution of your VM where your server is hosted (eg. Linux CentOS 7, Windows Server, ...)

## Customer infos
 - Your name
 - Email address
 - Skype ID
 - Discord ID
 - Rathena Profile
 - Herc.ws Profile

## Others
 - In game ID/Password for XPRO
 - FTP access to remote client
 
## Setup/Preparation
### Remote Client
__Requirement : valid SSL certificate (https) and if possible, create a subdomain for the remote client (eg : client.my-ro.com)__
 - Upload all your **GRF** _(not encrypted, not repacked and with version 0x200)_ and your **DATA.INI** in **/client/resources** on your FTP
 - Upload all the **BGM** in **/client/BGM/** on your FTP
 - Upload **Towninfo.lua** and **itemInfo.lua** in **/client/System/** on your FTP
 - Upload your **data folder** in **/client/data/** on your FTP
 - These files must exist in **GRF** or in **/client/data** (note: .lua files only, no .lub):
 1. data/luafiles514/lua files/signboardlist.lua
 2. data/luafiles514/lua files/admin/pcjobname.lua
 3. data/luafiles514/lua files/admin/pcjobnamegender.lua
 4. data/luafiles514/lua files/datainfo/accessoryid.lua
 5. data/luafiles514/lua files/datainfo/accname.lua
 6. data/luafiles514/lua files/datainfo/jobidentity.lua
 7. data/luafiles514/lua files/datainfo/jobname.lua
 8. data/luafiles514/lua files/datainfo/shadowtable.lua
 9. data/luafiles514/lua files/skillinfoz/skillid.lua
 10. data/luafiles514/lua files/skillinfoz/skilldescript.lua
 11. data/luafiles514/lua files/skillinfoz/skillinfolist.lua
 12. data/luafiles514/lua files/skillinfoz/skilltreeview.lua
 13. data/luafiles514/lua files/skillinfoz/jobinheritlist.lua
 14. data/luafiles514/lua files/stateicon/efstids.lua
 15. data/luafiles514/lua files/stateicon/stateiconinfo.lua
 16. data/luafiles514/lua files/stateicon/stateiconimginfo.lua
 - On your FTP, inside **/client/**, give access rights *660* (rw-rw----) to all files and *775* (rwxrwxr-x) to all folders (exceptions: index.php, .htaccess => *Access Rights: 664*). **- Be careful, error in access rights may break your remote client -**.
 - And give to all your GRF inside **/client/resources/** *access rights: 600* (visible by you only).
 - Login background is named **data/texture/À¯ÀúÀÎÅÍÆäÀÌ½º/bgi_temp.jpg** 
 - Loading screens are named **data/texture/À¯ÀúÀÎÅÍÆäÀÌ½º/loading<1-10>.jpg**
### Server-side
__Requirement : DNS (e.g.: xxxx.online-server.cloud) and valid SSL certificate (can be generated for free by zerossl.com)__
 - Install **NodeJs** on your VM
 - Run **npm install -g wsproxy** in a terminal on your VM
 - Open port **5999** on your Firewall

# Architecture examples
![Simple architecture](https://gitlab.com/xpro1/xpro/-/raw/master/simple_architecture.jpg?inline=false "Simple architecture")
![Complex architecture 1](https://gitlab.com/xpro1/xpro/-/raw/master/complex_architecture1.jpg?inline=false "Complex architecture 1")
![Complex architecture 2](https://gitlab.com/xpro1/xpro/-/raw/master/complex_architecture2.jpg?inline=false "Complex architecture 2")

# Q&A
## Certificates manipulation
1. Extract .key from .pem:
`openssl pkey -in cert.pem -out cert.key`

2. Extract .crt from .pem:
`openssl crl2pkcs7 -nocrl -certfile cert.pem | openssl pkcs7 -print_certs -out cert.crt`

## Update your remote client
In order to update your remote client, follow this steps :
 - Simply update your GRF located in **/client/resources** and/or your **BGM** files in **/client/BGM**.
 - If you modify **/client/System/itemInfo.lua**, then delete **/client/System/itemInfo.json**.
 - If you allowed GRF auto-extract, delete content of **/client/data/**.
 - Open **configs.php**, find **CLIENTVERSION** and increment number.
 This will automatically invalidate cache of your players **at next start of XPRO** and resources will be renewed.

**NB:** When first player ask for _itemInfo.lua_, it generates automatically _itemInfo.json_. Don't forget to put permission (access right) of itemInfo.json to **640**.

## Players have problems with remote client (missing textures)
On Background page, click on folder icon on bottom right corner and click "Clean client cache", then restart app.

## Players' UI is too big
You can reduce UI windows size on Settings.
Same for Joystick and shortcuts.

## Sometimes players get error : my_map.gnd is not found
Try to increase memory limit in `php.ini` to, at least, 256 MB :
	`ini_set('memory_limit', '256M');`
