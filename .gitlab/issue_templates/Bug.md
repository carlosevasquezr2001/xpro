# BUG
## Summary
(Summarize the bug encountered concisely)
## XPRO Version
(Give the XPRO version where your bug occurs)
## OS/Web browser version
(Android, iOS or Web. If web version, give OS name + browser name with version, example : Chrome 50.0 for Ubuntu, Firefox 59.3 (32 bits) for Win10, etc. )
## Steps to reproduce
(How one can reproduce the issue - this is very important. NOTE that a bug that cannot be reproduced will not be fixed)
## What is the current bug behavior?
(What actually happens)
## What is the expected correct behavior?
(What you should see instead)
## Screenshots
(Paste any relevant screenshot to illustrate your issue)